import sys
import ifttt.handler as ifttt

system = sys.argv[1]
command = sys.argv[2]

if system == "ifttt":
    ifttt.handle_command(command)
else:
    exit("Invalid system specified")
