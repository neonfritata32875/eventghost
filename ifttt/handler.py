import urllib.request as req
import time
import ifttt.properties as props


valid_commands = ["desk_light", "desk_light_off", "loft_camera_on", "loft_camera_off"]


def handle_command(command):
    if __validate_command__(command):
        __run_command__(command)
    else:
        exit("Invalid command")


def __run_command__(command):
    success = False
    retries = 2

    while (not success) and retries > 0:
        try:
            req.urlopen(
                "https://maker.ifttt.com/trigger/"+command+"/with/key/"+props.key)
            success = True
        except req.URLError:
            print("Could not connect")
            time.sleep(2)
            retries = retries - 1
            if retries == 0:
                exit("Maxed out retries, failing...")


def __validate_command__(command):
    if valid_commands.__contains__(command):
        return True
    else:
        return False
